# Amichay Test 2

__please see the fork [quick2a](https://bitbucket.org/vietthan/quick2a/src/main/) for use of header X-VIET and tests (Postman, pytest)__

## Clone, Configure, and Run the Two Apps
It would be best to follow the steps strictly.
### Clone
Move to desired location then run:

```bash
$ git clone git@bitbucket.org:vietthan/quick2.git
```

### Configure and Run the app on port 6543
Assuming installed `python3`. Run:

```bash
$ cd quick2/quick_tutorial2
$ export VENV=$(pwd)/env
$ python3 -m venv $VENV
$ cd ini
$ $VENV/bin/pip install -e .
$ $VENV/bin/pserve development.ini --reload &
```

### Configure and Run the app on port 6544
Assuming installed `python3`. Run:

```bash
$ cd ../../quick_tutorial3
$ export VENV=$(pwd)/env
$ python3 -m venv $VENV
$ cd ini
$ $VENV/bin/pip install -e .
$ $VENV/bin/pserve development.ini --reload &
```

### Result

Run in Chrome:

![hello world from 2 apps](run2apps.png)

or `curl` it:

![hello world from 2 apps with curl](curl2apps.png)

## Start and load Redis
Assuming redis is installed.

Start redis:

```bash
$ redis-server
```

Add keys to store host and ports:

```bash
$ redis-cli set port1 127.0.0.1:6543   
$ redis-cli set port2 127.0.0.1:6544
```  

## Run OpenResty and curl results
Assumed OpenResty is installed, check to make sure the OpenResty version of nginx is used with:

```bash
which nginx
```

Hopefully it points to the nginx executable that accompanies OpenResty.

return to the root of the project and run nginx on the accompanied file:

```bash
$ nginx -c $(pwd)/nginx-redis.conf 
```

Use `curl` to confirm results:

```bash
$ curl --user-agent port1 localhost:80
$ curl --user-agent port2 localhost:80
```

You should get:

![change the user-agent value with redis key](useragent2apps.png)


## Postman Testing

[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/18769888-5223a6f5-30f8-4675-8ca7-1beb7642a698?action=collection%2Ffork&collection-url=entityId%3D18769888-5223a6f5-30f8-4675-8ca7-1beb7642a698%26entityType%3Dcollection%26workspaceId%3D00bb2e01-23cb-4a09-8ade-c054ec7c907a)